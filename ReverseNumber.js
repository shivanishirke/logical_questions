//  Find the reverse of the number

let num = 345678;
let result = 0;
while(num>0){
        remainder = num%10;
        num = Math.floor(num/10);
        result = result * 10 + remainder;
}
console.log(result);