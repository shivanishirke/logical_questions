//  Find the reverse of the string


//with function
function reverseString(str) {
            let newString = "";
            for(i = str.length - 1; i>=0; i--){
                  newString += str[i];
               }
            return newString;    
      }
      
      const result = reverseString("Shivani");
      console.log(result)


    
//without function

    let name = "Shubbu"
    let newString = "";
    for(i= name.length - 1;i>=0;i--){
            newString += name[i]
    }
    console.log(name)
