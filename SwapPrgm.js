//Swap Values without using any variable a=10, b=12

//Solution 1: 
 a = 20;
 b = 30;
[a,b] = [b,a]
console.log(a,b)

//Solution 2:
 
a=20;
b=30;

a = a + b;
b = a - b;
a = a - b;
console.log(a,b)
